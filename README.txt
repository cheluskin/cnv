Проверенная система Ubuntu 16.04 x84_64, рекомендуется использовать ее.

Заходим в папку и запускаем скрипт:
bash convert.sh -d=/images_folder_path -s=/sound_file_path -q=1080 -bg=/background_video_file_path -l=/logo_path -o=/out_mp4_file_path

-d - обязательный параметр, папка с изображениями. Убедитесь, что в ней только файлы с изображениями, скрипт заберет все
-s - звуковой файл
-q - разрешение. Допустимо 2 режима - 720 и 1080. Если этот параметр вообще не указать то будет 720
-bg - фоновый видеофайл.
-l - файл логотипа, png с прозрачностью
-o - файл с готовым mp4, если не указать то будет out.mp4 в текущей папке

Для включения кодирования на видеокарте nvidia 1080 и других семейства Pascal.
1. Убедитесь что у вас установлены последние драйвера видеокарты.
2. Убедитесь что у вас установлена последняя версия CUDA
поставить можно так:

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
sudo apt-get update
sudo apt-get install cuda

3. Когда все поставлено и преезагружено добавляем в строку скрипта параметр --nv
Если видите такое
"[h264_nvenc @ 0x2a254c0] Cannot init CUDA" - значит есть проблема с CUDA или драйвером.



