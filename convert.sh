#!/usr/bin/env bash
for i in "$@"
do
case $i in
    -d=*|--dir=*)
    DIR="${i#*=}"
    shift
    ;;
    -o=*|--out=*)
    OUT="${i#*=}"
    shift
    ;;
    -s=*|--sound=*)
    SOUND="${i#*=}"
    shift
    ;;
    -q=*|--quality=*)
    QUALITY="${i#*=}"
    shift
    ;;
    -bg=*|--background=*)
    BG="${i#*=}"
    shift
    ;;
    -l=*|--logo=*)
    LOGO="${i#*=}"
    shift
    ;;
    --debug)
    debug=YES
    shift
    ;;
    --nv)
    NVIDIA=YES
    shift
    ;;
    *)
    echo "Unknown option $i"
    ;;
esac
done


if [ ${NVIDIA} ]; then
nvidia_encoder="-c:v h264_nvenc -surfaces 2";
fi

if [ ${debug} ]; then  echo "Finish parameters parsing: DIR=$DIR SOUND=$SOUND QUALITY=$QUALITY"; fi

if [ -z "${DIR}" ];
then echo "Error: parameter --dir contains an invalid value ${DIR}, please specify the directory";
exit 1;
fi

use_background=YES;
if [ -z "${BG}" ];
then echo "Warning: parameter --background not specified and background disabled";
use_background=NO;
fi

use_logo=YES;
if [ -z "${LOGO}" ];
then echo "Warning: parameter --logo not specified and logo disabled";
use_logo=NO;
fi

use_sound=YES;
if [ -z "${SOUND}" ];
then echo "Warning: parameter --sound not specified and sound disabled";
use_sound=NO;
fi

if [ -z "${QUALITY}" ];
then echo "Warning: parameter --quality not specified and we set default value 720";
QUALITY="720";
fi

if [ -z "${OUT}" ];
then echo "Warning: parameter --out not specified and we set default value out.mp4";
OUT="out.mp4";
fi

if ! [[ "$QUALITY" == "720" || "$QUALITY" == "1080" ]];
then echo "Error: parameter --quality contains an invalid value $QUALITY, only those values ​​are allowed 720 1080";
exit 1;
fi

if [ "$QUALITY" = "720" ]; then
vw="1280";
vh="720";
needscalebgto720="scale=1280x720,";
fi

if [ "$QUALITY" = "1080" ]; then
vw="1920";
vh="1080";
fi


if [ ${debug} ]; then  echo "Start folder parsing"; fi

ffmpeg_filter="";
if [ ${use_background} = YES ]; then
ffmpeg_filter+="movie=${BG}:loop=0,${needscalebgto720}setpts=N/FRAME_RATE/TB[bg];";
alpha=":alpha=1";
fi

if [ ${use_logo} = YES ]; then
ffmpeg_filter+="movie=${LOGO}:loop=0,setpts=N/FRAME_RATE/TB[logo];";
fi

ffmpeg_filter_concat_list="";
ffmpeg_input="";

count1=0;
for i in $DIR/*;
do

ffmpeg_input+="-loop 1 -t 3 -i ${i} ";
ffmpeg_filter+="[$count1:v]format=yuv420p,scale=iw*5:ih*5:out_range=pc,pad=if(lt(iw/5\,${vw})\,if(lt(ih/5\,${vh})\,iw*1.1*(${vw}/(iw/5))\,iw*2.21)\,iw*1.3):if(lt(iw/5\,${vw})\,if(lt(ih/5\,${vh})\,ih*1.1*(${vw}/(iw/5))*((iw/ih)/1.7777)\,ih*2.21*((iw/ih)/1.7777))\,ih*1.3*((iw/ih)/1.7777)):(ow-iw)/2:(oh-ih)/2:color=black@0,zoompan=z='min(zoom+0.00133,1.1)':x='iw/2-(iw/zoom/2)':y='ih/2-(ih/zoom/2)':d=75:s=hd${QUALITY}:fps=25,trim=duration=3,fade=t=in:st=0:d=0.5${alpha},fade=t=out:st=2.5:d=0.5${alpha}[v$count1];"
ffmpeg_filter_concat_list+="[v$count1]";
count1=$((count1+1));
done
audiotrimduration=$((count1*3));
audiofadeoutstart=$((audiotrimduration-1));

if [ ${use_sound} = YES ]; then
ffmpeg_filter+="amovie=${SOUND}:loop=0,afade=in:st=0:d=2,afade=out:st=${audiofadeoutstart}:d=1,asetpts=N/SR/TB,atrim=duration=${audiotrimduration}[sound];";
fi

ffmpeg_filter+="${ffmpeg_filter_concat_list}concat=n=${count1}:v=1:a=0[v]";
lastv="[v]";

if [ ${use_background} = YES ]; then
ffmpeg_filter+=";[bg][v]overlay=shortest=1[bgv]";
lastv="[bgv]";
fi

if [ ${use_logo} = YES ]; then
ffmpeg_filter+=";${lastv}[logo]overlay=shortest=1[bgvlogo]";
lastv="[bgvlogo]";
fi


ffmpeg_options="-y -benchmark -threads 0 ${ffmpeg_input} -filter_complex ${ffmpeg_filter} -map ${lastv} -threads 0 ${nvidia_encoder} ${OUT}";

if [ ${use_sound} = YES  ]; then
ffmpeg_options="-y -benchmark -threads 0 ${ffmpeg_input} -filter_complex ${ffmpeg_filter} -map ${lastv} -map [sound] -threads 0 ${nvidia_encoder} -strict experimental ${OUT}";
fi

if [ ${debug} ]; then
echo "ffmpeg $ffmpeg_options ";
fi

./bin/ffmpeg $ffmpeg_options;



